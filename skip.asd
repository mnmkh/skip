(in-package :cl-user)

(asdf:defsystem :skip
    :name "skip"
    :description "Lisp utility. skip."
    :version "0.1"
    :author "mnmkh"
    :serial T
    :encoding :utf-8
    :pathname "src/"
    :components 
              ((:file "packages")
               (:file "skip")
               (:file "anaphoric")
	       (:file "string")
               (:file "destructuring")
               (:file "match")
	       (:file "mop-util"))
    :depends-on (:alexandria
		 :cl-ppcre))

(asdf:defsystem :skip-test
  :version "0.1"
  :serial T
  :encoding :utf-8
  :pathname "t/"  
  :components ((:file "string-t"))
  :depends-on (:skip :fiveAM))

(defmethod perform ((op asdf:test-op) (system (eql (asdf:find-system :skip))))
  (funcall (intern (string :run!) (string :it.bese.FiveAM)) :skip))
